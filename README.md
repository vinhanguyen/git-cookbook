# Create new branch from commit

git checkout _branch_  
git checkout _commit_  
git checkout -b _new-branch_  
git push -u origin _new-branch_  

# See what changed in new branch
git diff _old-branch_ _new-branch_
